<?php
namespace App\Controller;

use App\Entity\Category;
use App\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParserController extends Controller
{
  private $linkToPageWithCategories, $schemeAndHost, $selectorForCategories, $maximumNumberOfCategoriesToScan,
    $selectorForPageTitles, $selectorForPageDescriptions, $maximumNumberOfPagesToScan;

  private function getContent(string $url)
  {
    $curlHandler = curl_init();
    curl_setopt($curlHandler, CURLOPT_URL, $url);
    curl_setopt($curlHandler, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curlHandler, CURLOPT_HEADER, false);
    $response = curl_exec($curlHandler);
    curl_close($curlHandler);

    return $response;
  }

  private function getSchemeAndHost(string $url): string {
    $urlParts = parse_url($url);
    $scheme = isset($urlParts['scheme']) ? $urlParts['scheme'] . '://' : '';
    $host = isset($urlParts['host']) ? $urlParts['host'] : '';

    return $scheme . $host;
  }

  private function getCategories(): array {
    $content = $this->getContent($this->linkToPageWithCategories);

    return (new Crawler($content))
      ->filter($this->selectorForCategories)
      ->reduce(function (Crawler $anchor, int $index) {
        return $index < $this->maximumNumberOfCategoriesToScan
          || $this->maximumNumberOfCategoriesToScan === 0;
      })->each(function (Crawler $anchor, int $index): Category {
        if (strpos($link = trim($anchor->attr('href')), $this->schemeAndHost) !== 0) {
          $link = $this->schemeAndHost . $link;
        }

        return (new Category())->setName(trim($anchor->text()))->setLink($link);
      }
    );
  }

  private function getPages(array $categories): array {
    $pages = [];

    foreach ($categories as $category) {
      $content = $this->getContent($category->getLink());
      $categoryId = $category->getId();
      $pages = array_merge($pages, (new Crawler($content))
        ->filter($this->selectorForPageTitles)
        ->reduce(function (Crawler $anchor, int $index) {
          return $index < $this->maximumNumberOfPagesToScan
            || $this->maximumNumberOfPagesToScan === 0;
        })->each(function (Crawler $anchor) use ($categoryId): Page {
          if (strpos($link = trim($anchor->attr('href')), $this->schemeAndHost) !== 0) {
            $link = $this->schemeAndHost . $link;
          }

          $content = $this->getContent($link);
          $description = (new Crawler($content))->filter($this->selectorForPageDescriptions)->text();

          return (new Page())->setCategoryId($categoryId)->setTitle(trim($anchor->text()))
            ->setDescription($description)->setLink($link);
        }
      ));
    }

    return $pages;
  }

  private function saveEntities(array $entities): array {
    $entityManager = $this->getDoctrine()->getManager();

    foreach ($entities as &$entity) {
      $entityManager->persist($entity);
      $entityManager->flush();
    }

    return $entities;
  }

  private function truncateTables(array $tableNamesToTruncate) {
    $entityManager = $this->getDoctrine()->getManager();
    $connection = $entityManager->getConnection();
    $platform = $connection->getDatabasePlatform();

    foreach ($tableNamesToTruncate as $tableNameToTruncate) {
      $connection->executeUpdate($platform->getTruncateTableSQL($tableNameToTruncate));
    }
  }

  public function index()
  {
    $requestData = Request::createFromGlobals()->request;
    $this->linkToPageWithCategories = $requestData->get('linkToPageWithCategories');
    $this->selectorForCategories = $requestData->get('selectorForCategories');
    $this->maximumNumberOfCategoriesToScan = (int) $requestData->get('maximumNumberOfCategoriesToScan', 0);
    $this->selectorForPageTitles = $requestData->get('selectorForPageTitles');
    $this->selectorForPageDescriptions = $requestData->get('selectorForPageDescriptions');
    $this->maximumNumberOfPagesToScan = (int) $requestData->get('maximumNumberOfPagesToScan', 0);

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');

    $errorCode = null;

    if (empty($this->linkToPageWithCategories) || empty($this->selectorForCategories)
      || empty($this->selectorForPageTitles) || empty($this->selectorForPageDescriptions)) {
        $errorCode = 1;
    } else if (filter_var($this->linkToPageWithCategories, FILTER_VALIDATE_URL) === false) {
      $errorCode = 2;
    }

    if (!is_null($errorCode)) {
      $response->setContent(json_encode(['response' => ['error' => ['code' => $errorCode]]]));
      $response->setStatusCode('400');

      return $response;
    }

    $this->schemeAndHost = $this->getSchemeAndHost($this->linkToPageWithCategories);
    $this->truncateTables(['category', 'page']);
    $categories = $this->saveEntities($this->getCategories());
    $pages = $this->saveEntities($this->getPages($categories));

    $response->setContent(json_encode(['response' => [
      'numberOfSavedCategories' => count($categories),
      'numberOfSavedPages' => count($pages)
    ]]));

    return $response;
  }
}
