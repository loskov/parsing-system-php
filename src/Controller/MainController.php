<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class MainController
{
  public function index()
  {
    return new Response(
<<<HTML
<!DOCTYPE html>
<html>
  <head>
    <title>Система парсинга сайтов</title>
    <style>
    input {
      width: 500px;
    }
    </style>
  </head>
  <body>
    <h1>Система парсинга сайтов</h1>
    <form name="sendDataForm">
      <h2>Категории</h2>
      <p>
        Адрес страницы с категориями:<br />
        <input name="linkToPageWithCategories" value="https://symfony.com/blog/" />
      </p>
      <p>
        Селектор CSS для ссылок на категории:<br />
        <input name="selectorForCategories" value="ul.submenu__nav > li > a" />
      </p>
      <p>
        Максимальное число сканируемых категорий (0 &mdash; без ограничения):<br />
        <input name="maximumNumberOfCategoriesToScan" value="1">
      </p>
      <h2>Страницы</h2>
      <p>
        Селектор CSS для ссылок на страницы:<br />
        <input name="selectorForPageTitles" value=".post__excerpt > h2 > a" />
      </p>
      <p>
        Селектор CSS для содержимого страниц:<br />
        <input name="selectorForPageDescriptions" value=".post__content" />
      </p>
      <p>
        Максимальное число сканируемых страниц в каждой категории (0 &mdash; без ограничения):<br />
        <input name="maximumNumberOfPagesToScan" value="1">
      </p>
      <p>
        <button onclick="sendData()" type="button">Запустить парсинг</button>
      </p>
      <div id="result"></div>
    </form>
    <script>
    var resultElement = document.getElementById('result');

    function sendData() {
      if (window.XMLHttpRequest) {
        var request = new XMLHttpRequest();
      }

      resultElement.innerHTML = 'Пожалуйста, подождите...';

      request.open('POST', '/parser', true);
      request.send(new FormData(document.forms['sendDataForm']));
      request.onreadystatechange = function () {
        if (request.readyState !== 4) {
          return;
        }

        if (request.status >= 200 && request.status < 300 || request.status === 304) {
          var data = JSON.parse(request.responseText);
          resultElement.innerHTML = '<h2>Результат</h2>'
            + '<p>Число сохранённых категорий: ' + data.response.numberOfSavedCategories + '.<br />'
            + 'Число сохранённых страниц: ' + data.response.numberOfSavedPages + '.</p>';
        } else if (request.status !== 0) {
          var data = JSON.parse(request.responseText);
          resultElement.innerHTML = '<h2>Результат</h2>'
            + 'Произошла ошибка. Код: ' + data.response.error.code + '.';
        }
      }
    }
    </script>
  </body>
</html>
HTML
     );
  }
}
